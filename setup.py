from setuptools import setup

setup(
    name="data-structures",
    description="Collection of Classic Data Structures: Implemented in Python",
    version=0.1,
    author="Selena Flannery",
    license="MIT",
    py_modules=[],
    package_dir={"": "src"},
    install_requires=["future"],
    extras_require{"test": ["pytest", "pytest-xdist", "tox"]},
)
